import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestProvider } from '../../providers/rest';
import { Geolocation } from '@ionic-native/geolocation/ngx';


@IonicPage()
@Component({
  selector: 'page-cafe-insert',
  templateUrl: 'cafe-insert.html',
})
export class CafeInsertPage {
  name:any;
  category:any;
  
  // model: Customer;

  constructor(
    public geo:Geolocation,
    public rest:RestProvider,
    public navCtrl: NavController, public navParams: NavParams) {
    this.name = this.navParams.data.name;
    this.category = this.navParams.data.category;
    // this.model = new Customer();

    //Carregar disciplina se editar.
    // if (this.navParams.data.id) {
    //   this.customerProvider.get(this.navParams.data.id)
    //     .then((result: any) => {
    //       this.model = result;
    //     })
    // }

  }

  ionViewDidLoad() { }

  save() {
    console.log(this.name);
    console.log(this.category);
    this.geo.getCurrentPosition({enableHighAccuracy:true}).then((success)=>{
      this.rest.insertNewCafe({position:{lat:success.coords.latitude,lng:success.coords.longitude},name:this.name,category:this.category})
      
    });
    
  }


}
