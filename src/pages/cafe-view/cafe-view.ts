import { Component } from '@angular/core';
import { NavController, ModalController, ToastController,LoadingController, AlertController, NavParams, IonicPage, Events } from 'ionic-angular';
import { CustomerProvider, GmailUser } from '../../providers/customer/customer'
import { CafeInsertPage } from '../cafe-insert/cafe-insert';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import 'hammerjs';
import { GalleryModal } from 'ionic-gallery-modal';
import { RestProvider } from '../../providers/rest';
import { GooglePlus } from '@ionic-native/google-plus/ngx';

@IonicPage()
@Component({
    selector: 'page-cafe-view',
    templateUrl: 'cafe-view.html'
})
export class CafeViewPage {
  Arr = Array; //Array type captured in a variable
  selectedTab:any;
  comment:any;
  commentUrl:any;
  ratingValue:any;
  loading:any;
  myLocation:any;
  meters:number;
  markerGroup:any;
  radiusGroup:any;
  currentGroup:any;
  reviews:any=[];
  userExists:any;
  place:any;
  cafeEntered:any;

  
//   user:any;

  constructor(
    public toastCtrl: ToastController,
    public navCtrl: NavController,
    public loadingCtrl:LoadingController,
    private customerProvider: CustomerProvider,
    private modalCtrl: ModalController,
    private alertCtrl: AlertController,
    private camera: Camera,
    public navParams: NavParams,
    private restProvider:RestProvider,
    private googlePlus:GooglePlus,
    public events: Events

  ) {
    events.subscribe('star-rating:changed', (starRating) => {this.ratingValue=starRating});
    this.place = {voted:false,id:this.navParams.get('id'),name:this.navParams.get('name'),category:this.navParams.get('category')};
    this.customerProvider.getPlace(this.place.id).then((res)=>{
        if (res!=null)
        this.place.voted = true;
    });
    this.selectedTab = 0;
    this.presentLoading("");
    this.cafeEntered = "Dodali ste Vaš komentar i ocenu za ovaj kafić, hvala!";
    this.comment="";
    this.commentUrl = "";
    this.ratingValue=0;
    this.getAllReviews();
     }
  changeTab(tab) {
    this.selectedTab=tab;
  }
  ionViewDidEnter() {
    console.log(this.navParams.data);
    
    
  }
  alertToast(title) {
    let toast = this.alertCtrl.create({
      title: title
    });
    toast.present();
  }
  insertComment(id) {
      console.log(this.ratingValue);
      console.log(this.comment);
      if (this.ratingValue==0) {
          this.alertToast("Molimo Vas, unesite ocenu kafe klikom na zvezdice!");
          return;
      }
      // if (this.comment=="") {
      //   this.alertToast("Molimo Vas, unesite komentar u tekstualno polje!");
      //   return;
      // }
      
      this.customerProvider.insertPlace(this.place.id);
      this.customerProvider.getAll()
      .then((result: any[]) => {
        
        if (result.length==0) {
            this.googleLogin();
            return;
        }
        var data = {"response":this.comment,"image":this.commentUrl,"rating":this.ratingValue,"user":{"id":null,"email":result[0].email,"fullName":result[0].fullname,"imageUrl":result[0].url}}
        this.restProvider.insertNewComment(data,id);
        this.place.voted = true;
      })
      
  }
  report(id) {
    this.restProvider.reportCafe(id);
  }
  googleLogin() {
     this.googlePlus.login({})
    .then(res => {
      if (!this.userExists) {
        let gmailUser = new GmailUser();
        gmailUser.email = res.email;
        gmailUser.url = res.imageUrl;
        gmailUser.fullname = res.displayName;
        var data = {"response":this.comment,"image":this.commentUrl,"rating":this.ratingValue,"user":{"id":null,"email":gmailUser.email,"fullName":gmailUser.fullname,"imageUrl":gmailUser.url}}
        this.customerProvider.insert(gmailUser);
        this.restProvider.insertNewComment(data,this.place.id);
        this.place.voted = true;
      }
    })
    .catch(err => alert(err));
  }
  presentLoading(title) {
    this.loading = this.loadingCtrl.create({
      content: title
    });
    this.loading.present();
  }

  addCafe() { this.navCtrl.push(CafeInsertPage,{name:"",category:""}); }

  editCustomer(id: number) { this.navCtrl.push(CafeInsertPage, { id: id }); }

  getAllReviews() {
      var that = this;
    this.restProvider.getReviews(this.place.id)
      .then((result: any[]) => {
        that.reviews = result;
        that.loading.dismiss();
      })
    }
    onSegmentChanged(event) {
        this.selectedTab = event.index;
      }

    novaFoto() {
        this.abrirCamera();
    }
    
      //-----CHAMAR_CAMERA--------------------------------------------------------------------------
      abrirCamera() { //Abrir e Salvar foto no banco.
        const options: CameraOptions = {
          quality: 75,
          // destinationType: this.platform.is('ios') ? this.camera.DestinationType.FILE_URI : this.camera.DestinationType.DATA_URL,
          destinationType: this.camera.DestinationType.DATA_URL,
          encodingType: this.camera.EncodingType.JPEG,
          mediaType: this.camera.MediaType.PICTURE,
          targetWidth: 300,
          targetHeight: 300,
          correctOrientation: true,
          saveToPhotoAlbum: false
        }
        //this.presentLoading("Waiting for best GPS accuracy");
        var that = this;
        that.camera.getPicture(options)
          .then((imageData) => {
            let base64Image = 'data:image/png;base64,' + imageData;
            var thatt = that;
            thatt.commentUrl = base64Image;
    
          }, (error) => {
            this.ionViewDidEnter();
          });
      }

      openGallery(img) {
        let modal = this.modalCtrl.create(GalleryModal, {
          photos: [{url:img}],
          initialSlide: 0,
          closeIcon: 'back'
        });
        modal.present();
     }
  

}

