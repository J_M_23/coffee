import { Component } from '@angular/core';
import { CustomerProvider, GmailUser } from '../../providers/customer/customer'
import { CafeInsertPage } from '../cafe-insert/cafe-insert';
import { NavController,LoadingController } from 'ionic-angular';
import leaflet from 'leaflet';
import 'hammerjs';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { RestProvider } from '../../providers/rest';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { CafeViewPage } from '../cafe-view/cafe-view';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  Arr = Array;
  // @ViewChild('map') mapContainer: ElementRef;
  map: any;
  // public disciplinas: any[] = [{"id": "1", "nome": "Programação IV", "professor": "José Eduardo Nunes Lino" },
  // {"id": "2", "nome": "Empreendedorismo", "professor": "Carlos Silla" }];
  public customers: any[] = [];
  selectedTab:any;
  public customersArray: any[] = [];
  public photos: any = [];
  public imageUrl: string;
  public currentLng: any;
  public currentLat: any;
  loading:any;
  myLocation:any;
  meters:number;
  markerGroup:any;
  radiusGroup:any;
  currentGroup:any;
  places:any=[];
  logedin:any=false;
  isMapLoaded:any=false;
  user:any;
  userExists:any;
  coffeeIcon = leaflet.icon({
    iconUrl: 'assets/css/images/icon-leaflet-coffee.png',
    iconSize:     [45, 45] // size of the icon
  });
  currentLocationIcon = leaflet.icon({
    iconUrl: 'assets/css/images/currentLocation.png',
    iconSize:     [45, 45] // size of the icon
  });
  

  constructor(
    public navCtrl: NavController,
    public loadingCtrl:LoadingController,
    private customerProvider: CustomerProvider,
    private geolocation:Geolocation,
    private restProvider:RestProvider,
    private googlePlus:GooglePlus

  ) {
      this.userExists = false;
      this.selectedTab = 0;
      this.markerGroup = leaflet.featureGroup();
      this.radiusGroup = leaflet.featureGroup();
      this.currentGroup = leaflet.featureGroup();
     }
  changeTab(tab) {
    this.selectedTab=tab;
  }
  ionViewDidEnter() {
    var that = this;
    that.presentLoading("Tražimo mesta u Vašoj blizini");
      that.reloadCurrentLocation();
      that.meters = 100;
  }
  googleLogin() {
     this.googlePlus.login({})
    .then(res => {
      if (!this.userExists) {
        let gmailUser = new GmailUser();
        gmailUser.email = res.email;
        gmailUser.url = res.imageUrl;
        gmailUser.fullname = res.displayName;
        this.customerProvider.insert(gmailUser);
      }
      //alert(user)
    }
    )
    .catch(err => alert(err));
  }
  presentLoading(title) {
    this.loading = this.loadingCtrl.create({
      content: title
    });
    this.loading.present();
  }

  addCafe() { this.navCtrl.push(CafeInsertPage,{name:"",category:""}); }

  cafeView(place) {this.navCtrl.push(CafeViewPage,{id:place.id,name:place.name,category:place.category}); }

  editCustomer(id: number) { this.navCtrl.push(CafeInsertPage, { id: id }); }

  pushData() {
    this.customersArray = [{"name":"tessst2","images":[{"img":"sss"},{"img":"sss2"}]}];
    this.restProvider.post(this.customersArray);
  }
  getAllCustomers() {
    this.customerProvider.getAll()
      .then((result: any[]) => {
        this.customers = result;
      })
  }
  onInputChange(event: any) {
    this.meters = event.value;
  }
  //-----NOVA_FOTO----------------------------------------------------------------------------
  

  onSegmentChanged(event) {
    console.log(event);
    this.selectedTab = event.index;
    if (event.index==1) {
      if (this.isMapLoaded) {
        
        return;
      }
     setTimeout(() => {
       this.loadmap();
     }, 1000);

    }
  }
  reloadAndLoadMapCurrent() {
    this.presentLoading("");
      this.restProvider.getPlaces(this.meters,this.currentLat,this.currentLng).then(
        (result) => {
          this.places = result;
          for (var i = 0;i<this.places.length;i++) {
            this.places[i].percentage = this.places[i].rating*100/5;
          }
          console.log(result);
          if (this.isMapLoaded) {
            this.markerGroup.clearLayers();
            this.radiusGroup.clearLayers();
            this.currentGroup.clearLayers();
            this.reloadMap(this.currentLat,this.currentLng);
          }
          else {
            this.loading.dismiss();
          }
          
        }
      );
  }
  reloadAndLoadMap() {
    this.presentLoading("");
    this.reloadCurrentLocation();
  }
  reloadCurrentLocation() {
    this.geolocation.getCurrentPosition({enableHighAccuracy:true}).then((success)=>{
      this.myLocation=success.coords;
      this.restProvider.getPlaces(this.meters,success.coords.latitude,success.coords.longitude).then(
        (result) => {
          this.places = result;
          for (var i = 0;i<this.places.length;i++) {
            this.places[i].percentage = this.places[i].rating*100/5;
          }
          this.currentLng = success.coords.longitude;
          this.currentLat = success.coords.latitude;
          // this.loading.dismiss();
          console.log(result);
          if (this.isMapLoaded) {
            this.markerGroup.clearLayers();
            this.radiusGroup.clearLayers();
            this.currentGroup.clearLayers();
            this.reloadMap(this.currentLat,this.currentLng);
          }
          else {
            this.loading.dismiss();
          }
          
        }
      );
    });
  }
  sliderDown() {
    if (this.meters==100) return;
    this.meters-=100;
    this.reloadAndLoadMapCurrent();
  }
  sliderUp() {
    if (this.meters==400) return;
    this.meters+=100;
    this.reloadAndLoadMapCurrent();
  }
  reloadMap(lat,lng) {
      var bounds=[];
      this.currentGroup.addLayer(this.initCLMarker(this.myLocation.latitude,this.myLocation.longitude));
      if (this.places.length===0) {

        this.map.setView({lat:lat,lng:lng}, 19);this.loading.dismiss();return;
      }
      //this.markerGroup.clearLayers();
      
      for(var i=0;i<this.places.length;i++) {
        var latitude = this.places[i].position.lat;
        var longitude = this.places[i].position.lng;
        this.markerGroup.addLayer(this.initMarker(latitude, longitude,this.places[i].name));
        //this.markerGroup.addLayer(this.initLabelMarker(latitude, longitude,this.places[i].name));
        bounds[i] = [];
        bounds[i].push([latitude,longitude]);
      }
      var polygon = leaflet.polygon(bounds);
      // this.map.addLayer(this.markerGroup);
      // $('#map .marker-cluster').first().click();
      
      this.map.fitBounds(polygon.getBounds());
      this.loading.dismiss();
      

  }
  loadmap() {
    
    console.log("loadmap");
    var bounds=[];
    this.map = leaflet.map("map").fitWorld();
    leaflet.tileLayer('https://api.tiles.mapbox.com/v4/mapbox.streets/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoibWFya29qZXJlbWlub3YyMyIsImEiOiJjanI5Y2g2ZnMwMnpwNGFvNmg4ZzQ5em1rIn0.6IIdTSP_iNWAROZN8LhSZw', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      // useCache: true,
      crossOrigin: true,
      minZoom:10,
      maxZoom:19
    }).addTo(this.map);
    this.map.locate({
      setView: true
    }).on('locationfound', (e) => {
      if (this.isMapLoaded) {
        this.map.addLayer(this.markerGroup);
        this.reloadMap(e.latlng.lat,e.latlng.lng);
        return;
      }
      this.isMapLoaded=true;
      
      for(var i=0;i<this.places.length;i++) {
        // var image = this.places[i].img;
        var latitude = this.places[i].position.lat;
        var longitude = this.places[i].position.lng;
        // var accuracy = this.places[i].accuracy;
        // circleGroup.addLayer(this.initCircle(image, latitude, longitude, accuracy));
        this.markerGroup.addLayer(this.initMarker(latitude, longitude, this.places[i].name));
        //this.markerGroup.addLayer(this.initLabelMarker(latitude, longitude,this.places[i].name));
        bounds.push([latitude,longitude]);
      }
      this.map.addLayer(this.markerGroup);
      this.map.addLayer(this.radiusGroup);
      this.map.addLayer(this.currentGroup);
      
      }).on('locationerror', (err) => {
        alert(err.message);
    })
    this.map.on('load', this.onMapLoad);
    var that = this;
    this.map.on('click', function(e) {
      that.presentLoading("Loading");
      that.restProvider.getPlaces(that.meters,e.latlng.lat,e.latlng.lng).then(
        (result) => {
          that.places = result;
          for (var i = 0;i<that.places.length;i++) {
              that.places[i].percentage = that.places[i].rating*100/5;
          }
          that.currentLat = e.latlng.lat;
          that.currentLng = e.latlng.lng;
          if (that.markerGroup!==undefined) {
            that.markerGroup.clearLayers();
            that.radiusGroup.clearLayers();
            that.currentGroup.clearLayers();
          }
          that.reloadMap(e.latlng.lat,e.latlng.lng);
          // that.loading.dismiss();
          //that.map.setView({lat:e.latlng.lat,lng:e.latlng.lng}, 18);
        }
      );
      
      
      
  });
    
  
  }
  getWidth(rat) {
    return rat*100/5;
  }
  placeClick(place) {
    this.changeTab(1);
    if (this.radiusGroup!=undefined && this.currentGroup!=undefined) {
      this.radiusGroup.clearLayers();
      this.currentGroup.clearLayers();
    }
    if (!this.isMapLoaded) {
      
      setTimeout(() => {
        this.radiusGroup.addLayer(this.initRadius(place.position.lat,place.position.lng,place.name));
        this.currentGroup.addLayer(this.initCLMarker(this.myLocation.latitude,this.myLocation.longitude));
        this.map.setView({lat:place.position.lat,lng:place.position.lng}, 19);
        //this.markerGroup.addLayer(this.initRadius(place.position.lat,place.position.lat,name));
      }, 2000);
    }
    else {
      this.radiusGroup.addLayer(this.initRadius(place.position.lat,place.position.lng,place.name));
      this.currentGroup.addLayer(this.initCLMarker(this.myLocation.latitude,this.myLocation.longitude));
      this.map.setView({lat:place.position.lat,lng:place.position.lng}, 19);
    }
    
  }
  onMapLoad() {
    setTimeout(() => {
      this.map.invalidateSize();
    }, 0);
 }
 initRadius(latitude, longitude, name) {
  let marker: any = leaflet.marker([latitude, longitude])
  return marker;
}
 initMarker(latitude, longitude, name) {
  let marker: any = leaflet.marker([latitude, longitude],{title:name,icon:this.coffeeIcon});
  marker.bindPopup(name,{permanent:true}).openPopup();
  return marker;
}
initCLMarker(latitude, longitude) {
  let marker: any = leaflet.marker([latitude, longitude],{title:"Ovde si",icon:this.currentLocationIcon});
  marker.bindPopup("Ovde si",{permanent:true}).openPopup();
  return marker;
}
initLabelMarker(latitude, longitude, text) {

var divIcon = leaflet.divIcon({ 
  html: '<div style="text-align: center;color: white;width: fit-content;background-color: blue;font-size: 1.2em;margin-top: -79px;border-style: groove;">'+text+'</div>'
  });
  return leaflet.marker(new leaflet.LatLng(latitude, longitude), {icon: divIcon });
}



}

