import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule, LOCALE_ID, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MyApp } from './app.component';

//Pages
import { CafeInsertPage } from '../pages/cafe-insert/cafe-insert';
import { CafeViewPage } from '../pages/cafe-view/cafe-view';
import { HomePage } from '../pages/home/home';
import { RestProvider } from '../providers/rest';
import {HttpClientModule} from '@angular/common/http';
import { IonicStorageModule } from '@ionic/storage';
import { HttpModule } from '@angular/http';
//Providers
import { DatabaseProvider } from '../providers/database/database';
import { CustomerProvider } from '../providers/customer/customer';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { StarRatingModule } from 'ionic3-star-rating';
import { Network } from '@ionic-native/network/ngx';
// import { Diagnostic } from '@ionic-native/diagnostic/ngx';


//Plugin
import { SQLite } from '@ionic-native/sqlite/ngx';
import { Camera } from '@ionic-native/camera/ngx';
import { Base64 } from '@ionic-native/base64/ngx';
import { File } from '@ionic-native/file/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as ionicGalleryModal from 'ionic-gallery-modal';
import { HAMMER_GESTURE_CONFIG } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations'
import {GestureConfig,MatSlideToggleModule, MatButtonModule, MatSelectModule, MatTabsModule, MatListModule, MatChipsModule, MatIconModule, MatCardModule, MatGridListModule, MatSliderModule} from '@angular/material'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CafeInsertPage,
    CafeViewPage
  ],
  imports: [
    BrowserModule,
    StarRatingModule,
    MatGridListModule,
    MatSliderModule,
    MatSlideToggleModule,
    HttpClientModule,
    MatButtonModule,
    MatCardModule,
    MatSelectModule,
    MatIconModule,
    MatListModule,
    MatTabsModule,
    MatChipsModule,
    HttpModule,
    IonicModule.forRoot(MyApp, {
      tabsPlacement: 'top',
      backButtonText: '',
      scrollPadding: false,
      scrollAssist: true,
      autoFocusAssist: false
     }),
     IonicStorageModule.forRoot({
      name: 'tilesDb',
         driverOrder: ['indexeddb', 'sqlite', 'websql']
    }),
    BrowserAnimationsModule,
     ionicGalleryModal.GalleryModalModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CafeInsertPage,
    CafeViewPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    SQLite,
    {provide: LOCALE_ID, useValue: 'pt-BR'},
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    { provide: HAMMER_GESTURE_CONFIG, useClass: GestureConfig },
    RestProvider,
    DatabaseProvider,
    CustomerProvider,
    Camera,
    Base64,
    File,
    StatusBar,
    Geolocation,
    GooglePlus,
    Network
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA,
    NO_ERRORS_SCHEMA]
})
export class AppModule {}
