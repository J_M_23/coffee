import { Component } from '@angular/core';
import { Platform, Alert,ToastController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';

import { HomePage } from '../pages/home/home';
import { DatabaseProvider } from '../providers/database/database';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/merge';
import 'rxjs/add/observable/fromEvent';
import 'rxjs/add/operator/map'
import { Network } from '@ionic-native/network/ngx';
// import { Diagnostic } from '@ionic-native/diagnostic';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any = null;
  connectedOnStart = true;

  constructor(public toastCtrl: ToastController,private network:Network,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, dbProvider: DatabaseProvider
    ) {
    platform.ready().then(() => {
      statusBar.styleLightContent();
      let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
        let toast = this.toastCtrl.create({
          message: "Niste konektovani na Internet! Morate biti kako biste koristili aplikaciju!",
          duration: 6000
        });
        toast.present();
      });
      
      // stop disconnect watch
     
      // watch network for a connection
      let connectSubscription = this.network.onConnect().subscribe(() => {
        let toast = this.toastCtrl.create({
          message: "Konektovani ste!",
          duration: 3000
        });
        toast.present();
        this.rootPage = HomePage;
        if (!this.connectedOnStart) {
          this.connectedOnStart = true;
          dbProvider.createDataBase()
        .then(() => {
          this.homePage(splashScreen);
        })
        .catch(() => {
          this.homePage(splashScreen);
        });
        }
      });
      if (!this.isConnected()) {
        let toast = this.toastCtrl.create({
          message: "Niste konektovani na Internet! Morate biti kako biste koristili aplikaciju!",
          duration: 8000
        });
        toast.present();
        this.connectedOnStart = false;
      }
      else {
        // this.homePage(splashScreen);
          dbProvider.createDataBase()
        .then(() => {
          this.homePage(splashScreen);
        })
        .catch(() => {
          this.homePage(splashScreen);
        });
      }  
    });

  }
  isConnected(): boolean {
    let conntype = this.network.type;
    return conntype && conntype !== 'unknown' && conntype !== 'none';
  }
  private homePage(splashScreen: SplashScreen) {
    splashScreen.hide();
    this.rootPage = HomePage;
  }

}

