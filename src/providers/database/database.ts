import { Injectable } from '@angular/core';
import{SQLiteObject,SQLite} from '@ionic-native/sqlite/ngx'


@Injectable()
export class DatabaseProvider {

  constructor(private sqlite: SQLite) { }

  public getDB() {
    return this.sqlite.create({
      name: 'data.db',
      location: 'default'
    })
  }

  public createDataBase() {
    return this.getDB()
      .then((db: SQLiteObject) => {
        return this.createTables(db);
      })
      .catch(e => console.error(e));
  }

  //HELPERS
  private createTables(db: SQLiteObject) {
    return db.sqlBatch(['CREATE TABLE IF NOT EXISTS user (id INTEGER primary key AUTOINCREMENT NOT NULL, url TEXT, fullname TEXT, email TEXT)',
    'CREATE TABLE IF NOT EXISTS place (id INTEGER primary key AUTOINCREMENT NOT NULL, placeId NUMERIC)']);
  }


}
