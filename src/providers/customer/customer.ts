import { Injectable } from '@angular/core';
import { SQLiteObject } from '@ionic-native/sqlite';
import { DatabaseProvider } from '../database/database';


@Injectable()
export class CustomerProvider {

  constructor(private dbProvider: DatabaseProvider) { }

  //-----INSERIR-------------------------------------------------------
  public insert(customer: GmailUser) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO user (fullname, email, url) VALUES (?, ?, ?)';
        let data = [customer.fullname, customer.email, customer.url];
        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public insertPlace(placeId: number) {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'INSERT INTO place (placeId) VALUES (?)';
        let data = [placeId];
        return db.executeSql(sql, data)
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }

  public getPlace(id: number) {
      return this.dbProvider.getDB()
        .then((db: SQLiteObject) => {
          let sql = 'SELECT * FROM place WHERE placeId = ?';
          let dados = [id];
  
          return db.executeSql(sql, dados)
            .then((data: any) => {
              if (data.rows.length > 0) {
                let item = data.rows.item(0);
                return {id:item.id,placeId:item.placeId};
              }
              return null;
            })
            .catch((e) => console.error(e));
        })
        .catch((e) => console.error(e));
    }

  //-----ATUALIZAR-----------------------------------------------------
  // public update(customer: Customer) {
  //   return this.dbProvider.getDB()
  //     .then((db: SQLiteObject) => {
  //       let sql = 'UPDATE customer SET name = ?, owner = ?, address = ?, tel = ?, region = ?, town = ?, email = ? WHERE id = ?';
  //       let data = [customer.name, customer.owner, customer.address, customer.tel, customer.region, customer.town, customer.email, customer.id];
  //       return db.executeSql(sql, data)
  //         .catch((e) => console.error(e));
  //     })
  //     .catch((e) => console.error(e));
  // }

  // //-----REMOVER-------------------------------------------------------
  // public remove(id: number) {
  //   return this.dbProvider.getDB()
  //     .then((db: SQLiteObject) => {
  //       let sql = 'DELETE FROM customer WHERE id = ?';
  //       let data = [id];
  //       return db.executeSql(sql, data)
  //         .catch((e) => console.error(e));
  //     })
  //     .catch((e) => console.error(e));
  // }

  //-----GET----------------------------------------------------------
  // public get(id: number) {
  //   return this.dbProvider.getDB()
  //     .then((db: SQLiteObject) => {
  //       let sql = 'SELECT * FROM customer WHERE id = ?';
  //       let dados = [id];

  //       return db.executeSql(sql, dados)
  //         .then((data: any) => {
  //           if (data.rows.length > 0) {
  //             let item = data.rows.item(0);
  //             let customer = new Customer();
  //             customer.id = item.id;
  //             customer.name = item.name;
  //             customer.owner = item.owner;
  //             customer.region = item.region;
  //             customer.tel = item.tel;
  //             customer.town = item.town;
  //             customer.email = item.email;
  //             customer.address = item.address;
  //             return customer;
  //           }

  //           return null;
  //         })
  //         .catch((e) => console.error(e));
  //     })
  //     .catch((e) => console.error(e));
  // }

  //-----GET(ALL)----------------------------------------------------
  public getAll() {
    return this.dbProvider.getDB()
      .then((db: SQLiteObject) => {
        let sql = 'SELECT * FROM user';

        return db.executeSql(sql, null)
          .then((data: any) => {
            if (data.rows.length > 0) {
              let customers: any[] = [];
              for (var i = 0; i < data.rows.length; i++) {
                var disciplina = data.rows.item(i);
                customers.push(disciplina);
              }
              return customers;
            } else {
              return [];
            }
          })
          .catch((e) => console.error(e));
      })
      .catch((e) => console.error(e));
  }


}


export class GmailUser {
  id: number;
  url: string;
  email: string;
  fullname: string;
}