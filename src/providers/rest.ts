import { Injectable} from '@angular/core';
import { AlertController } from 'ionic-angular';
import { Http , Headers, RequestOptions} from '@angular/http';
import { HttpClient } from '@angular/common/http';

/*
  Generated class for the RestProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class RestProvider {

  constructor(private toastCtrl:AlertController,public http: Http,public httpClient: HttpClient) {
    console.log('Hello RestProvider Provider');
  }
  post(customers) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });


    this.http.post("https://nawec.plusplus.rs/api/customer/save", customers, requestOptions)
      .subscribe(data => {
        console.log(data);
       }, error => {
        console.log(error);
      });
  }
  alertToast(title) {
    let toast = this.toastCtrl.create({
      title: title
    });
    toast.present();
  }
  insertNewCafe(cafe:any) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });
    // alert(JSON.stringify(cafe));

    this.http.post("http://rr.plusplus.rs:9989/public/api/place/addNew", cafe, requestOptions)
      .subscribe(data => {
        this.alertToast("Hvala, pregledaćemo Vaš predlog uskoro!");
       }, error => {
        console.log(error);
      });
  }
  reportCafe(placeId:any) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });
    // alert(JSON.stringify(cafe));

    this.http.post("http://rr.plusplus.rs:9989/public/api/place/reportCafe?placeId="+placeId, {}, requestOptions)
      .subscribe(data => {
        this.alertToast("Hvala, obrisaćemo ovo mesto u najkraćem roku!");
       }, error => {
        console.log(error);
      });
  }
  insertNewComment(cafe:any,placeId) {
    var headers = new Headers();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    const requestOptions = new RequestOptions({ headers: headers });
    // alert(JSON.stringify(cafe));

    this.http.post("http://rr.plusplus.rs:9989/public/api/place/addReview?placeId="+placeId, cafe, requestOptions)
      .subscribe(data => {
        this.alertToast("Hvala, Vaš komentar će se prikazati u narednih nekoliko minuta!");
       }, error => {
        console.log(error);
      });
  }
  getPlaces(radius,lat,lng) {
    return new Promise(resolve => {
    this.httpClient.get('http://rr.plusplus.rs:9989/public/api/place/getRadius?distance='+radius+"&lat="+lat+"&lng="+lng)
    .subscribe(data => {
        resolve(data);
       }, error => {
        console.log(error);
      });
    });
  }
  getReviews(placeId) {
    return new Promise(resolve => {
    this.httpClient.get('http://rr.plusplus.rs:9989/public/api/place/reviews?placeId='+placeId)
    .subscribe(data => {
        resolve(data);
       }, error => {
        console.log(error);
      });
    });
  }
  posts(customers) {
    this.http.post('https://nawec.plusplus.rs/api/customer/save', customers)
    .subscribe(data => {
        console.log(data);
       }, error => {
        console.log(error);
      });

}
}
